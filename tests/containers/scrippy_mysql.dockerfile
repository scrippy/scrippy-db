FROM mysql:8.0
ENV MYSQL_ROOT_PASSWORD root
COPY init_mysql.sql /docker-entrypoint-initdb.d/init.sql
EXPOSE 3306
