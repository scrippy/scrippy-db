FROM postgres:13.13-bookworm
ENV POSTGRES_USER postgres
ENV POSTGRES_PASSWORD postgres
ENV POSTGRES_DB scrippy
COPY init_pgsql.sql /docker-entrypoint-initdb.d/init.sql
EXPOSE 5432
