CREATE USER 'scrippy'@'%' IDENTIFIED BY 'scrippy';
CREATE DATABASE scrippydb;
GRANT ALL PRIVILEGES ON scrippydb.* TO 'scrippy'@'%';
USE scrippydb
CREATE TABLE users(
  id          SERIAL PRIMARY KEY   NOT NULL,
  name        TEXT                 NOT NULL,
  givenname   TEXT                 NOT NULL,
  password    TEXT                 NOT NULL
);
INSERT INTO users (name, givenname, password) values('Fink', 'Larry', 'D34dP4rr0t');
INSERT INTO users (name, givenname, password) values('Vercotti', 'Luiggi', '5p4n15h1nqu1s1t10n');
