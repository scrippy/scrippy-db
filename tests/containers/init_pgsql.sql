CREATE USER scrippy WITH PASSWORD 'scrippy';
CREATE DATABASE scrippydb;
\connect scrippydb
CREATE TABLE users(
  id          SERIAL PRIMARY KEY   NOT NULL,
  name        TEXT                 NOT NULL,
  givenname   TEXT                 NOT NULL,
  password    TEXT                 NOT NULL
);
INSERT INTO users (name, givenname, password) values('Fink', 'Larry', 'D34dP4rr0t');
INSERT INTO users (name, givenname, password) values('Vercotti', 'Luiggi', '5p4n15h1nqu1s1t10n');
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO scrippy;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO scrippy;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public TO scrippy;
GRANT ALL PRIVILEGES ON DATABASE scrippydb TO scrippy;
