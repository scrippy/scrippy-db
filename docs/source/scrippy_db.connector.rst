scrippy\_db.connector package
=============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   scrippy_db.connector.mysql
   scrippy_db.connector.oracle
   scrippy_db.connector.pgsql
   scrippy_db.connector.sqlite3

Module contents
---------------

.. automodule:: scrippy_db.connector
   :members:
   :undoc-members:
   :show-inheritance:
